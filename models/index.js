const Sequelize = require('sequelize');
const pg = require('pg');
const db = {};
let config = {};
if (process.env.NODE_ENV === 'test') {
  config = require(__dirname + '/../config/config.json')[process.env.NODE_ENV];
}
let sequelize = new Sequelize(
  process.env.DB_NAME || config.database,
  process.env.DB_USERNAME || config.username,
  process.env.DB_PASSWORD || config.password,
  {
    host: process.env.DB_HOST || config.host,
    dialect: process.env.DB_DIALECT || config.dialect,
    schema: process.env.DB_SCHEMA || config.schema,
    dialectModule: pg
  }
);

sequelize.authenticate()
.then(() => {
    console.log('inside try sequelize.authenticate: ');
  })
  .catch(() => {
    console.log('after all try sequelize.authenticate: ');
  });

import { Verification } from './verification';
Verification.init(sequelize, Sequelize);
db['Verification'] = Verification;

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

export default db;