import { Model, DataTypes } from 'sequelize';

export class Verification extends Model {
  static init(sequelize) {
    return super.init({
      dna: DataTypes.JSONB,
      isMutant: DataTypes.BOOLEAN,
      mutantMessage: {
        type: DataTypes.VIRTUAL,
        get () {
          let isMutant = this.isMutant ? 'is' : 'is not';
          return `The dna ${isMutant} a mutant`;
        }
      }
    },
      {
        sequelize,
        tableName: 'Verifications',
        timestamps: true
      });
  };
}
