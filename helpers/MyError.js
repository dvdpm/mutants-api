export class MyError extends Error {
  constructor(statusCode, errors) {
    super(Array.isArray(errors) ? 'Ocurrió un error' : errors);
    if (!Array.isArray(errors)) {
      this.message = errors;
      errors = [{
        message: errors
      }];
    }
    this.statusCode = statusCode;
    this.errors = errors;
  }
}
