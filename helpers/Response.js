export class Response {
  constructor() {
    this.responseData = {
      statusCode: 200,
      body: {},
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
      }
    };
  }
  send() {
    console.log(`Returning response with status ${this.responseData.statusCode}`, this.responseData);
    return this.responseData;
  }

  status(status) {
    this.responseData.statusCode = status;
    return this;
  }

  successful() {
    this.status(200);
    return this;
  }

  body(body) {
    this.responseData.body = JSON.stringify(body, null, 2);
    return this;
  }
}