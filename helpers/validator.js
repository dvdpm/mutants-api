import { validator } from 'indicative';
const { validate } = validator;

const _validate = async (body, rules) => {
  return new Promise(async (resolve, reject) => {
    body = JSON.parse(body || '{}');
    try {
      let data = await validate(body, rules, {}, { removeAdditional: true });
      resolve(data);
    } catch (errors) {
      reject({
        statusCode: 422,
        message: 'invalid data',
        errors
      });
    }
  });
};

export { _validate as validate };
