# Mutants API

## API to check if dna is mutant

### Base url endpoint

```js
https://t4y3lgqg13.execute-api.us-east-1.amazonaws.com/dev
```

### To check if dna is a mutant make a `POST` request to:

```js
/mutants
```

The body must be a JSON with the following format

```js
{
  "dna": ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]
}
```

### To get stats for validations, make a `GET` request to:

```js
/stats
```

The response will be

```js
{
  "message": "The dna stats",
  "count_human_dna": 1,
  "count_mutant_dna": 1,
  "ratio": 1
}
```
