import { getSequences } from './helper';
import { validate } from './helpers/validator';
import { Response } from './helpers/Response';
import { MyError } from './helpers/MyError';
import index from './models/index';
const Verification = index.Verification;
const Op = index.Sequelize.Op;

export async function isMutant(event) {
  try {
    let data = await validate(event.body, {
      dna: 'required|array'
    });
    let combinations = getSequences(data.dna);
    data.isMutant = combinations.length >= 2;
    let existVerification = await Verification.findOne({
      where: {
        dna: {
          [Op.contains]: data.dna
        }
      }
    });
    if (existVerification) {
      return new Response()
        .status(existVerification.isMutant ? 200 : 300)
        .body({
          message: existVerification.mutantMessage
        })
        .send();
    }
    let verification = await Verification.build(data).save();
    if (!verification.isMutant) {
      throw new MyError(403, 'The dna is not a mutant');
    }
    return new Response()
      .successful()
      .body({
        message: 'The dna IS a mutant'
      })
      .send();
  } catch (error) {
    return new Response()
      .status(error.statusCode || 500)
      .body({
        message: error.message,
        error: error.message,
        errors: error.errors
      })
      .send();
  }
}

export async function mutantStats(event) {
  try {
    let verifications = await Verification.findAndCountAll({
      attributes: ['isMutant'],
      group: ['isMutant']
    });
    let mutants = verifications.count.find(group => group.isMutant);
    let noMutants = verifications.count.find(group => !group.isMutant);
    let count_mutant_dna = parseInt(mutants ? mutants.count : 0);
    let count_no_mutant_dna = parseInt(noMutants ? noMutants.count : 0);
    let count_human_dna = count_mutant_dna + count_no_mutant_dna;
    let ratio = parseFloat(count_mutant_dna / count_human_dna) || 0;

    return new Response()
      .successful()
      .body({
        message: 'The dna stats',
        count_human_dna,
        count_mutant_dna,
        ratio
      })
      .send();
  } catch (error) {
    return new Response()
      .status(error.statusCode || 500)
      .body({
        message: error.message,
        error: error.message,
        errors: error.errors
      })
      .send();
  }
}
