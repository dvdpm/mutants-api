export function getCombinations (dna) {
  let combinations = [];
  for (let index = 0; index < dna.length; index++) {
    combinations.push(dna[index]);
    let vertical = [];
    let obliqueRight = [];
    let obliqueLeft = [];
    for (let secondIndex = 0; secondIndex < dna[index].length; secondIndex++) {
      vertical.push(dna[secondIndex][index]);
      obliqueRight.push(dna[secondIndex][index + secondIndex]);
      if (index + secondIndex < dna.length && index !== 0) {
        obliqueLeft.push(dna[index + secondIndex][secondIndex]);
      }
    }
    if (obliqueLeft.join('')) {
      combinations.push(obliqueLeft.join(''));
    }
    combinations.push(vertical.join(''));
    combinations.push(obliqueRight.join(''));
  }
  return combinations;
}

export function getSequences(data) {
  let sequences = [];
  for (const group of getCombinations(data)) {
    let coincidences = 0;
    for (let secondIndex = 1; secondIndex < group.length; secondIndex++) {
      if (group[secondIndex - 1] === group[secondIndex]) {
        coincidences++;
      }
      if (coincidences >= 3) {
        sequences.push(group);
        break;
      }
    }
    coincidences = 0;
  }
  return sequences;
}
