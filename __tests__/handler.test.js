import * as handler from '../handler';

describe('check if is a mutant', () => {
  test('it is a mutant', async () => {
    const event = {
      body: JSON.stringify({
        dna: ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]
      })
    };

    const response = await handler.isMutant(event);
    expect(response.statusCode).toEqual(200);
  });

  test('it is not a mutant', async () => {
    const event = {
      body: JSON.stringify({
        dna: ["ATGCGA", "CAGTGC", "TTATGT", "AGAXGG", "CCXCTA", "TCACTG"]
      })
    };

    const response = await handler.isMutant(event);
    expect(response.statusCode).toEqual(403);
  }, 5000);

  test('it validate dna', async () => {
    const event = {
      body: JSON.stringify({
        dna: 'invalid data'
      })
    };

    const response = await handler.isMutant(event);
    expect(response.statusCode).toEqual(422);
  });

});
