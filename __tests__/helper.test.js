import * as helper from '../helper';

describe('get combinations', () => {
  test('it has all combinations', async () => {
    let dna = ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]
    let combinations = helper.getCombinations(dna)
    expect(combinations).toEqual(expect.arrayContaining(['AAAATG']))
    expect(combinations.length).toBe(23)
  })
})

describe('get-sequences', () => {
  test('it return 3 combinations', () => {
    let combinations = ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]
    let sequences = helper.getSequences(combinations)
    expect(sequences.length).toBe(3)
    expect(sequences).toEqual(expect.arrayContaining(['AAAATG', 'GGGGTT', 'CCCCTA']))
  })

  test('it return zero combinations', () => {
    let combinations = ["ATGCGA", "ACTACT", "RAAATG", "CAGTGC", "CTACT", "TATGCC"]
    let sequences = helper.getSequences(combinations)
    expect(sequences.length).toBe(0)
  })
})
